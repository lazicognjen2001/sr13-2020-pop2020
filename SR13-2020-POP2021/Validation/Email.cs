﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR13_2020_POP2021.Validation
{
    public class Email : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Contains("@") && value.ToString().EndsWith(".com"))
            {
                return new ValidationResult(true, "");
            }
            return new ValidationResult(false, "e-mail is not valid");
        }
    }
}

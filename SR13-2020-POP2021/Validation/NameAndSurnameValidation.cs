﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR13_2020_POP2021.Validation
{
    class NameAndSurnameValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
            
        {
            Console.WriteLine(value.ToString());
            if (!value.ToString().Equals(""))
            {
                return new ValidationResult(true, "");
            }
            return new ValidationResult(false, "Data is not valid");
        }
    }
}

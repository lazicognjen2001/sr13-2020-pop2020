﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR13_2020_POP2021.Validation
{
    class IdValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Length == 13)
            {
                return new ValidationResult(true, "");
            }
            return new ValidationResult(false, "ID is not valid");
        }
    }
}

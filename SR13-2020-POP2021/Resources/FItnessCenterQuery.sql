-- Query for creating database for Fitness Center project (SR13-2020-POP2021)

--create database FitnessCenter
/*
create table Addresses (
	Id Int Primary Key,
	Country Varchar(30) Not Null,
	City Varchar(20) Not Null,
	Adress Varchar(30) Not Null,
	PostalCode Int Not Null,
	IsActive Bit Not Null
)

Insert into Addresses Values
(10000, 'Srbija', 'Novi Sad', 'Bulevar Despoda Stefana', 5, 1)
Insert into Addresses Values
(10001, 'Srbija', 'Novi Sad', 'Bulevar Oslobođenja', 6, 1)
Insert into Addresses Values
(10002, 'Srbija', 'Novi Sad', 'Jurija Gagarina', 7, 1)
Insert into Addresses Values
(10003, 'Srbija', 'Novi Sad', 'Jevrejska', 8, 1)
Insert into Addresses Values
(10004, 'Srbija', 'Novi Sad', 'Sime Matavulja', 9, 1)
Insert into Addresses Values
(10005, 'Srbija', 'Novi Sad', 'Ćirpanova', 10, 1)
Insert into Addresses Values
(10006, 'Srbija', 'Novi Sad', 'Šekspirova', 12, 1)

create table GeneralInfo (
	Id Int Primary Key,
	Name Varchar(40) Not Null,
	AddressId Int,
	foreign key(AddressId) references Addresses (Id)
)

Insert into GeneralInfo Values
(900, 'Maximus', 10000)

create table Users (
	Id BigInt Primary Key,
	Ime Varchar(20) Not Null,
	Prezime Varchar (20) Not Null,
	Email Varchar(30) Not Null,
	Password Varchar(30) Not Null,
	Gender Varchar(7) check (Gender in('Male', 'Female')),
	UserType Varchar(11) check (UserType in ('Client', 'Instructor', 'Admin')),
	AddressId Int Not Null,
	foreign key(AddressId) references Addresses (Id),
	IsActive Bit Not Null
)

Insert into Users Values 
(13051991145917, 'Joe', 'Koeman', 'joe@gmail.com', 'joekoeman', 'Male', 'Admin', 10001, 1)
Insert into Users Values 
(11021991155927, 'Mark', 'Ollson', 'mark@gmail.com', 'markollson', 'Male', 'Instructor', 10002, 1)
Insert into Users Values 
(12031995165937, 'Lili', 'Deler', 'lili@gmail.com', 'lilideler', 'Female', 'Instructor', 10003, 1)
Insert into Users Values 
(13041998175947, 'Tomas', 'Matkins', 'tomas@gmail.com', 'tomasmatkins', 'Female', 'Client', 10004, 1)
Insert into Users Values 
(14051999185957, 'Will', 'Jonson', 'will@gmail.com', 'willjonson', 'Male', 'Client', 10005, 1)
Insert into Users Values 
(15061999195967, 'Veronica', 'Maksimilian', 'veronica@gmail.com', 'veronicamaksimilian', 'Female', 'Client', 10006, 1)

create table Trainings (
	Id Int Primary Key,
	UserId BigInt,
	foreign key(UserId) references Users (Id),
	InstructorId BigInt,
	foreign key(InstructorId) references Users (Id),
	TrainingTime Datetime Not Null,
	TrainingStatus Varchar(9) check (TrainingStatus in ('Booked', 'Free')),
	IsActive Bit Not Null
)

Insert into Trainings Values 
(20000, 13041998175947, 11021991155927, '2021-12-01 08:00', 'Booked', 1)
Insert into Trainings Values 
(20001, 13041998175947, 12031995165937, '2021-12-01 09:00', 'Booked', 1)
Insert into Trainings Values 
(20002, 14051999185957, 11021991155927, '2021-12-02 10:00', 'Booked', 1)
Insert into Trainings Values 
(20003, 14051999185957, 12031995165937, '2021-12-02 11:00', 'Booked', 1)
Insert into Trainings Values 
(20004, 15061999195967, 12031995165937, '2021-12-03 12:00', 'Booked', 1)
Insert into Trainings Values 
(20005, 15061999195967, 12031995165937, '2021-12-03 13:00', 'Booked', 1)
Insert into Trainings Values 
(20006, Null, 12031995165937, '2021-12-04 10:00', 'Free', 1)
Insert into Trainings Values 
(20007, Null, 12031995165937, '2021-12-04 11:00', 'Free', 1)
*/

select * from Users
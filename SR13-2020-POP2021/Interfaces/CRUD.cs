﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Interfaces
{
    public interface CRUD
    {
        ObservableCollection<Object> GetData();
        void ChangeELement();

         void DeleteElement();

         void CreateElement();
    }
}

﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.View.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Instructor
{
    /// <summary>
    /// Interaction logic for CreateTrainingWindow.xaml
    /// </summary>
    public partial class CreateTrainingWindow : Window
    {
        public CreateTrainingWindow()
        {
            InitializeComponent();
            List<string> vr = new List<string>();
            for( int i = 6; i <= 22; i++)
            {
                string hourTime;
                if(i < 10)
                {
                    hourTime = "0" + i;
                } else
                {
                    hourTime = i.ToString();
                }

                vr.Add(hourTime + ":00");
                TimeComboBox.Items.Add(hourTime + ":00");
                Console.WriteLine("C");
            }
           
        }

        private void CreateTraining(object sender, RoutedEventArgs e)
        {

            try
            {

            
           DateTime date =(DateTime)DatePicker.SelectedDate;
            string time = TimeComboBox.SelectedItem.ToString();

            Console.WriteLine(date + " -- " + time);

            DateTime d = date.Date.AddHours(TimeComboBox.SelectedIndex + 6);
            Console.WriteLine(TimeComboBox.SelectedIndex + 6);
            Console.WriteLine(d);

            int newId = 0;
            foreach (Training tr in FitnessCenter.Instance.AllTrainings)
            {
                if(tr.Id >= newId)
                {
                    newId = tr.Id + 1;
                }
            }

            

            SR13_2020_POP2021.Model.Instructor instructor = (SR13_2020_POP2021.Model.Instructor)FitnessCenter.Instance.CurrentUser;

            Training training = new Training(newId, null, instructor, d, ETrainingStatus.Free);
            training.InstructorId = instructor.Id;
            FitnessCenter.Instance.AllTrainings.Add(training);
            TrainingCRUD.AddNewTrainingInDatabase(training);
            Console.WriteLine(training.Id);

            AllTrainingsInstructorWindow ad = new AllTrainingsInstructorWindow();
            ad.Show();
            this.Close();
            }
            catch (Exception)
            {

                MessageBox.Show("Please insert valid information");
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            AllTrainingsInstructorWindow all = new AllTrainingsInstructorWindow();
            all.Show();
            this.Close();
        }
    }
}

﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace SR13_2020_POP2021.View.Instructor
{
    /// <summary>
    /// Interaction logic for AllTrainingsInstructorWindow.xaml
    /// </summary>
    public partial class AllTrainingsInstructorWindow : Window
    {
        public AllTrainingsInstructorWindow()
        {
            InitializeComponent();
            Model.Instructor instructor = (Model.Instructor)FitnessCenter.Instance.CurrentUser;
            AllTrainingsDataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings.Where(o => o.Instructor == instructor);
            // AllTrainingsDataGrid.ItemsSource = trainings;//.Select(o => new { Id = o.Id, Instructor = o.Instructor.Name, Client = o.Client, Date = o.Date, Status = o.TrainingStatus }).ToList();

            InstructorComboBox.ItemsSource = FitnessCenter.Instance.AllUsers.Where(o => o.UserType == EUserType.Client);
         

        }

        private void DeleteTraining_Click(object sender, RoutedEventArgs e)
        {
            try { 
            object selected = AllTrainingsDataGrid.SelectedItem;
            Training treining = (Training)selected;

                if (treining.Client != null)
                {
                    MessageBox.Show("You can not delete this training");
                } else
                {
                    TrainingCRUD.DeleteTrainingFromDatabase(treining);
                    FitnessCenter.Instance.AllTrainings.Remove(treining);
                    Model.Instructor instructor = (Model.Instructor)FitnessCenter.Instance.CurrentUser;
                    AllTrainingsDataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings.Where(o => o.Instructor == instructor);
                }
            
            }
            catch
            {
                MessageBox.Show("Please select training");
            }

        }

        private void AddNewTermin_Click(object sender, RoutedEventArgs e)
        {
            CreateTrainingWindow create = new CreateTrainingWindow();
            create.Show();
            this.Close();

        }

        private void SearchClick_Click(object sender, RoutedEventArgs e)
        {
            Model.Instructor instructor = (Model.Instructor)FitnessCenter.Instance.CurrentUser;
            SR13_2020_POP2021.Model.Client client;
            Console.WriteLine(DatePicker.SelectedDate);
            List<Training> trainings = new List<Training>();

            try
            {
                client  = (SR13_2020_POP2021.Model.Client)InstructorComboBox.SelectedItem;
            }
            catch
            {
                client = null;
            }

            DateTime date;
            try
            {
                date = (DateTime)DatePicker.SelectedDate;
            } catch
            {
                date = new DateTime();
            }

            Console.WriteLine(date.Date);

            foreach (Training item in FitnessCenter.Instance.AllTrainings)
            {
                Console.WriteLine((item.Client == client || client == null) && (date == item.Date.Date || date == new DateTime()) && (item.Instructor.Id == instructor.Id));
                if ((item.Client == client || client == null) && (date == item.Date.Date || date == new DateTime()) && (item.Instructor.Id == instructor.Id)) 
                {
                    trainings.Add(item);
                }
            }
            AllTrainingsDataGrid.ItemsSource = trainings;

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            InstructorMainWindow instructorMainWindow = new InstructorMainWindow();
            instructorMainWindow.Show();
            this.Close();
        }
    }
}

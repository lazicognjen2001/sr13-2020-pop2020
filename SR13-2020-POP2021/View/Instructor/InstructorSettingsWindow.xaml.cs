﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Instructor
{
    /// <summary>
    /// Interaction logic for InstructorSettingsWindow.xaml
    /// </summary>
    public partial class InstructorSettingsWindow : Window
    {
        public InstructorSettingsWindow()
        {
            InitializeComponent();

            this.DataContext = FitnessCenter.Instance.CurrentUser;

            /*
            NameField.Text = FitnessCenter.Instance.CurrentUser.Name;
            SurnameField.Text = FitnessCenter.Instance.CurrentUser.Surname;
            EmailField.Text = FitnessCenter.Instance.CurrentUser.Email;
            IdField.Text = FitnessCenter.Instance.CurrentUser.Id.ToString();
            */

            if (FitnessCenter.Instance.CurrentUser.Gender == EGender.Male)
            {
                MaleRadioButton.IsChecked = true;
            }
            else
            {
                FemaleRadioButton.IsChecked = true;
            }

            IdField.Text = FitnessCenter.Instance.CurrentUser.Id.ToString();
        }

        private void Change_data(object sender, RoutedEventArgs e)
        {

            /*
            FitnessCenter.Instance.CurrentUser.Name = NameField.Text;
            FitnessCenter.Instance.CurrentUser.Surname = SurnameField.Text;
            FitnessCenter.Instance.CurrentUser.Email = EmailField.Text;
            */

            if (isValid())
            {

                if (FirstPasswordField.Password != null && FirstPasswordField.Password == FitnessCenter.Instance.CurrentUser.Password && SecondPasswordField.Password != null)
                {
                    FitnessCenter.Instance.CurrentUser.Password = SecondPasswordField.Password;
                }

                if (MaleRadioButton.IsChecked == true)
                {
                    FitnessCenter.Instance.CurrentUser.Gender = EGender.Male;
                }
                else
                {
                    FitnessCenter.Instance.CurrentUser.Gender = EGender.Female;
                }

                UserCRUD.UpdateUserInDatabase(FitnessCenter.Instance.CurrentUser);

                MessageBox.Show("Successfully changed informations");

                InstructorMainWindow clientMainWindow = new InstructorMainWindow();
                clientMainWindow.Show();
                this.Close();
            }


        }

        private void Back(object sender, RoutedEventArgs e)
        {
            InstructorMainWindow clientMainWindow = new InstructorMainWindow();
            clientMainWindow.Show();
            this.Close();
        }

        private void AddNewAddress(object sender, RoutedEventArgs e)
        {

        }

        private bool isValid()
        {
            bool valid = !System.Windows.Controls.Validation.GetHasError(EmailField)
                && !System.Windows.Controls.Validation.GetHasError(NameField)
                && !System.Windows.Controls.Validation.GetHasError(SurnameField)
                && !System.Windows.Controls.Validation.GetHasError(IdField);
            return valid;
        }
    }
}

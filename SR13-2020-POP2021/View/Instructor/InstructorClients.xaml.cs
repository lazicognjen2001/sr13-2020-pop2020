﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Instructor
{
    /// <summary>
    /// Interaction logic for InstructorClients.xaml
    /// </summary>
    public partial class InstructorClients : Window
    {
        public InstructorClients()
        {
            InitializeComponent();
            Model.Instructor currentUser = (Model.Instructor)FitnessCenter.Instance.CurrentUser;
            var instructortrainings = FitnessCenter.Instance.AllTrainings.Where(o => o.Instructor == currentUser);
            List<Model.Client> instructorUsers = new List<Model.Client>(); 
            foreach (Training item in instructortrainings)
            {
                if (item.Client != null)
                {
                    if (instructorUsers.Contains(item.Client))
                    {

                    }
                    else if (!instructorUsers.Contains(item.Client))
                    {
                        instructorUsers.Add((Model.Client)item.Client);
                    }
                }
            }
            AllInstructorUsersDataGrid.ItemsSource = instructorUsers;
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            InstructorMainWindow ins = new InstructorMainWindow();
            ins.Show();
            this.Close();
        }
    }
}

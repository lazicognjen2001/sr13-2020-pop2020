﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Instructor
{
    /// <summary>
    /// Interaction logic for InstructorMainWindow.xaml
    /// </summary>
    public partial class InstructorMainWindow : Window
    {
        public InstructorMainWindow()
        {
            InitializeComponent();
            NameAndSurname.Content = FitnessCenter.Instance.CurrentUser.Name + " " + FitnessCenter.Instance.CurrentUser.Surname;

        }

        public void YourTrainings(object sender, RoutedEventArgs e)
        {
            AllTrainingsInstructorWindow allTrainingsInstructorWindow = new AllTrainingsInstructorWindow();
            allTrainingsInstructorWindow.Show();
            this.Close();
        }

        public void Settings(object sender, RoutedEventArgs e)
        {
            InstructorSettingsWindow instructor = new InstructorSettingsWindow();
            instructor.Show();
            this.Close();

        }

        public void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void YourClients(object sender, RoutedEventArgs e)
        {
            InstructorClients ins = new InstructorClients();
            ins.Show();
            this.Close();
        }
    }
}

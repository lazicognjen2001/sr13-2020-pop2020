﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for EditTrainingWindow.xaml
    /// </summary>
    public partial class EditTrainingWindow : Window
    {
        Training trainingToEdit;
        public EditTrainingWindow(Training training)
        {
            InitializeComponent();
            trainingToEdit = training;
            InstructorComboBox.Items.Add(training.Instructor);
            InstructorComboBox.SelectedIndex = 0;

            DatePicker.SelectedDate = training.Date;

            TimeComboBox.Items.Add(training.Date.TimeOfDay);
            TimeComboBox.SelectedIndex = 0;

            for (int i = 7; i <= 22; i++)
            {
                string hourTime;
                if (i < 10)
                {
                    hourTime = "0" + i;
                }
                else
                {
                    hourTime = i.ToString();
                }

                //vr.Add(hourTime + ":00");
                TimeComboBox.Items.Add(hourTime + ":00");
            }

            foreach (User user in FitnessCenter.Instance.AllUsers)
            {
                if(user.UserType == EUserType.Instructor && !InstructorComboBox.Items.Contains(user))
                {
                    InstructorComboBox.Items.Add(user);
                }
            }

        }

        private void EditTraining(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime date = (DateTime)DatePicker.SelectedDate;
                string time = TimeComboBox.SelectedItem.ToString();

                DateTime d = date.Date.AddHours(TimeComboBox.SelectedIndex + 6);
                trainingToEdit.Date = d;

                SR13_2020_POP2021.Model.Instructor instructor = (Model.Instructor)InstructorComboBox.SelectedItem;

                trainingToEdit.InstructorId = instructor.Id;
                trainingToEdit.Instructor = instructor;
                TrainingCRUD.UpdateTrainingInDatabase(trainingToEdit);

                AllTrainingsWindow ad = new AllTrainingsWindow();
                ad.Show();
                this.Close();
            }
            catch
            {
                MessageBox.Show("Insert Valid information");
            }
        }
        
        private void Back(object sender, RoutedEventArgs e)
        {
            AllTrainingsWindow all = new AllTrainingsWindow();
            all.Show();
            this.Close();
        }
    }
}

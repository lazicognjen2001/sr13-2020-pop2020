﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AllAddresses.xaml
    /// </summary>
    public partial class AllAddresses : Window
    {
        public AllAddresses()
        {
            InitializeComponent();
            AllAddressesDataGrid.ItemsSource = FitnessCenter.Instance.AllAdresses;
        }

        private void AddAddress(object sender, RoutedEventArgs e)
        {
            AddAddressNonUser add = new AddAddressNonUser();
            add.Show();
            this.Close();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            AdminMainWindow admin = new AdminMainWindow();
            admin.Show();
            this.Close();
        }

        private void DeleteAddress(object sender, RoutedEventArgs e)
        {
            try
            {
                Adress addresToDelete = (Adress)AllAddressesDataGrid.SelectedItem;
                bool userHaveAddress = false;

                foreach (User user in FitnessCenter.Instance.AllUsers)
                {
                    if (user.Adress == addresToDelete)
                    {
                        userHaveAddress = true;
                    }
                }

                if (!userHaveAddress && addresToDelete != null)
                {
                    FitnessCenter.Instance.AllAdresses.Remove(addresToDelete);
                    AddressCRUD.DeleteAddressFromDatabase(addresToDelete);
                } else
                {
                    MessageBox.Show("User have assigned this addres, you can not delete it or address is not valid");
                }


            }
            catch 
            {

                MessageBox.Show("Please select valid address.");
            }
        }

        private void EditAddress(object sender, RoutedEventArgs e)
        {
            Adress address = (Adress)AllAddressesDataGrid.SelectedItem;
            if(address != null)
            {
                EditAddressWindow edit = new EditAddressWindow(address);
                edit.Show();
                this.Close();
            } else
            {
                MessageBox.Show("Please select address");
            }
            
        }
    }
}

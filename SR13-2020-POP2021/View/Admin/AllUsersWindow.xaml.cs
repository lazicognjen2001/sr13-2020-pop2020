﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AllUsersWindow.xaml
    /// </summary>
    public partial class AllAddressesWindow : Window
    {
        public AllAddressesWindow()
        {
            InitializeComponent();
            var allUsers = FitnessCenter.Instance.AllUsers.Where(o => o.IsActive = true);
            AllUsersDataGrid.ItemsSource = allUsers;

            _ = GenderCombo.Items.Add("Male");
            _ = GenderCombo.Items.Add("Female");
            _ = UserTypeBox.Items.Add("Client");
            _ = UserTypeBox.Items.Add("Admin");
            _ = UserTypeBox.Items.Add("Instructor");

        }

        private void Back(object sender, RoutedEventArgs e)
        {
            AdminMainWindow adminWIndow = new AdminMainWindow();
            adminWIndow.Show();
            this.Close();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {

            string name = NameField.Text;
            string surname = SurnameField.Text;
            string email = EmailField.Text;
            
            List<User> findedUsers = new List<User>();
           

            foreach  (User user in FitnessCenter.Instance.AllUsers)
            {

                bool info = (user.Name.Equals(name) || name == "") &&
                    (user.Surname.Equals(surname) || surname == "") &&
                    (user.Email.Equals(email) || email == "");

                

                bool gender = user.Gender.ToString().Equals(GenderCombo.SelectedItem) || GenderCombo.SelectedItem == null;
                bool userType = user.UserType.ToString().Equals(UserTypeBox.SelectedItem) || UserTypeBox.SelectedItem == null;



                if (
                    info && gender && userType
                    )
                {
                    findedUsers.Add(user);
                }
            }

            AllUsersDataGrid.ItemsSource = findedUsers;
        }

        private void AddNewUser_Click(object sender, RoutedEventArgs e)
        {
            AddNewUserWindow addUserWindow = new AddNewUserWindow();
            addUserWindow.Show();
            this.Close();
        }

        private void DeleteUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Int32 selectedRowCount = AllUsersDataGrid.SelectedIndex;
                Console.WriteLine(selectedRowCount);

                FitnessCenter.Instance.AllUsers[selectedRowCount].IsActive = false;
                UserCRUD.DeleteUserFromDatabase(FitnessCenter.Instance.AllUsers[selectedRowCount]);
                Console.WriteLine(FitnessCenter.Instance.AllUsers[selectedRowCount]);
                FitnessCenter.Instance.AllUsers.Remove(FitnessCenter.Instance.AllUsers[selectedRowCount]);
                var allUsers = FitnessCenter.Instance.AllUsers.Where(o => o.IsActive = true);
                AllUsersDataGrid.ItemsSource = allUsers;

            }
            catch
            {
                MessageBox.Show("Select user you want to delete.");
            }

        }

        private void EditUser(object sender, RoutedEventArgs e)
        {
            if ((User)AllUsersDataGrid.SelectedItem != null)
            {
                EditUserWindow edit = new EditUserWindow((User)AllUsersDataGrid.SelectedItem);
                edit.Show();
                this.Close();
            } else
            {
                MessageBox.Show("Please select user");
            }   
        }
    }
}

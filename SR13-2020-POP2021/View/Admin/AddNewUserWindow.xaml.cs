﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AddNewUserWindow.xaml
    /// </summary>
    public partial class AddNewUserWindow : Window
    {

        User newUser;
        public AddNewUserWindow()
        {
            InitializeComponent();
            newUser = new User();
            this.DataContext = newUser;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            /*
            string Name = NameField.Text;
            string Surname = SurnameField.Text;
            string Email = EmailField.Text;
            string FirstPassword = FirstPasswordField.Password.ToString();
            string SecondPassword = SecondPasswordField.Password.ToString();
            string Id = IdField.Text;
            */
            if (isValid())
            {

                string State = StateField.Text;
                string City = CityField.Text;
                string Street = StreetField.Text;
                string Number = NumberField.Text;
                EGender Gender = EGender.Male;
                EUserType userType = EUserType.Instructor;

                if ((bool)MaleRadioButton.IsChecked)
                {
                    Gender = EGender.Male;
                }
                else if ((bool)FemaleRadioButton.IsChecked)
                {
                    Gender = EGender.Female;
                }

                if ((bool)InstructorRadioButton.IsChecked)
                {
                    userType = EUserType.Instructor;
                }
                else if ((bool)AdminRadioButton.IsChecked)
                {
                    userType = EUserType.Admin;
                }

                //long IDLong = long.Parse(Id);
                Adress clientAdress = null;
                int newAddressID = 0;
                bool addressExist = false;
                foreach (Adress address in FitnessCenter.Instance.AllAdresses)
                {
                    if (State.Equals(address.State) && City.Equals(address.City) && Street.Equals(address.Street) && int.Parse(Number) == address.AdressNumber)
                    {
                        clientAdress = address;
                        addressExist = true;
                        break;
                    }
                    else
                    {
                        if (address.Id > newAddressID)
                        {
                            newAddressID = address.Id + 1;
                        }
                    }
                }
                if (!addressExist)
                {
                    clientAdress = new Model.Adress(newAddressID, State, City, Street, int.Parse(Number));
                    AddressCRUD.AddNewAddressInDatabase(clientAdress);
                }

                //Model.Client NewClient = new Model.Client(IDLong, Name, Surname, Email, FirstPassword, Gender, userType, clientAdress, true);
                newUser.UserType = userType;
                newUser.IsActive = true;
                newUser.Gender = Gender;
                newUser.Adress = clientAdress;
                newUser.AddressId = clientAdress.Id;
                newUser.Password = FirstPasswordField.Password;
                Console.WriteLine(newUser.ToString());
                Console.WriteLine(clientAdress.ToString());
                FitnessCenter.Instance.AllUsers.Add(newUser);
                FitnessCenter.Instance.CurrentUser = newUser;
                UserCRUD.AddNewUserInDatabase(newUser);

                AllAddressesWindow usersWindow = new AllAddressesWindow();
                usersWindow.Show();
                this.Close();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            AllAddressesWindow usersWindow = new AllAddressesWindow();
            usersWindow.Show();
            this.Close();
        }

        private bool isValid()
        {
            bool valid = !System.Windows.Controls.Validation.GetHasError(EmailField)
                && !System.Windows.Controls.Validation.GetHasError(NameField)
                && !System.Windows.Controls.Validation.GetHasError(SurnameField)
                && !System.Windows.Controls.Validation.GetHasError(IdField);
            return valid;
        }
    }
}

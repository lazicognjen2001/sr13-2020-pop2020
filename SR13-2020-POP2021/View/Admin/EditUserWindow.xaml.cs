﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for EditUserWindow.xaml
    /// </summary>
    public partial class EditUserWindow : Window
    {
        User userToEdit;
        public EditUserWindow(User user)
        {
            InitializeComponent();

            userToEdit = user;
            this.DataContext = userToEdit;

            /*
            NameField.Text = user.Name;
            SurnameField.Text = user.Surname;
            EmailField.Text = user.Email;
            */


            if(user.Gender == EGender.Male)
            {
                MaleRadioButton.IsChecked = true;
            } else
            {
                FemaleRadioButton.IsChecked = true;
            }
        }

        private void EditUser(object sender, RoutedEventArgs e)
        {
            /*
               
            try
            {
                userToEdit.Name = NameField.Text;
                userToEdit.Surname = SurnameField.Text;
                userToEdit.Email = EmailField.Text;

                if (MaleRadioButton.IsChecked == true)
                {
                    userToEdit.Gender = EGender.Male;
                }
                else
                {
                    userToEdit.Gender = EGender.Female;
                }
            

            } catch
            {
                MessageBox.Show("Error");
            }

            */
            UserCRUD.UpdateUserInDatabase(userToEdit);

            AllAddressesWindow all = new AllAddressesWindow();
            all.Show();
            this.Close();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            AllAddressesWindow all = new AllAddressesWindow();
            all.Show();
            this.Close();
        }
    }
}

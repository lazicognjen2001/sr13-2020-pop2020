﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for EditAddressWindow.xaml
    /// </summary>
    public partial class EditAddressWindow : Window
    {

        Adress addressToEdit;
        public EditAddressWindow(Model.Adress address)
        {
            InitializeComponent();

            
            addressToEdit = address;
            this.DataContext = addressToEdit;
            /*
            StateField.Text = address.State;
            CityField.Text = address.City;
            NumberField.Text = address.AdressNumber.ToString();
            StreetField.Text = address.Street;
            */

        }

        private void EditAddress(object sender, RoutedEventArgs e)
        {
            /*
            try
            {
                addressToEdit.State = StateField.Text;
                addressToEdit.City = CityField.Text;
                addressToEdit.AdressNumber = int.Parse(NumberField.Text);
                addressToEdit.Street = StreetField.Text;

                AddressCRUD.UpdateAddressInDatabase(addressToEdit);

                AllAddressesWindow all = new AllAddressesWindow();
                all.Show();
                this.Close();
            }
            catch
            {
                MessageBox.Show("Error");
            }
            */

            if (isValid())
            {
                AddressCRUD.UpdateAddressInDatabase(addressToEdit);

                AllAddresses all = new AllAddresses();
                all.Show();
                this.Close();
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            AllAddresses all = new AllAddresses();
            all.Show();
            this.Close();
        }

        private bool isValid()
        {
            bool valid = !System.Windows.Controls.Validation.GetHasError(StateField)
                && !System.Windows.Controls.Validation.GetHasError(StreetField)
                && !System.Windows.Controls.Validation.GetHasError(CityField)
                && !System.Windows.Controls.Validation.GetHasError(NumberField);
            return valid;
        }
    }
}

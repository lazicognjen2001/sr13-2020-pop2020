﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AdminMainWindow.xaml
    /// </summary>
    public partial class AdminMainWindow : Window
    {
        public AdminMainWindow()
        {
            InitializeComponent();
            NameAndSurname.Content = FitnessCenter.Instance.CurrentUser.Name + " " + FitnessCenter.Instance.CurrentUser.Surname;

        }

        public void AllUsers(object sender, RoutedEventArgs e)
        {
            AllAddressesWindow usersWindow = new AllAddressesWindow();
            usersWindow.Show();
            this.Close();
        }

        public void AllTrainings(object sender, RoutedEventArgs e)
        {
            AllTrainingsWindow trainingsWindow = new AllTrainingsWindow();
            trainingsWindow.Show();
            this.Close();

        }

        public void Settings(object sender, RoutedEventArgs e)
        {
            AdminSettingsWindow admin = new AdminSettingsWindow();
            admin.Show();
            this.Close();

        }

        public void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void AllAddresses(object sender, RoutedEventArgs e)
        {
            AllAddresses all = new AllAddresses();
            all.Show();
            this.Close();
        }
    }
}

﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AllTrainingsWindow.xaml
    /// </summary>
    public partial class AllTrainingsWindow : Window
    {
        public AllTrainingsWindow()
        {
            InitializeComponent();
            AllTrainings_DataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings;
            ClientComboBox.ItemsSource = FitnessCenter.Instance.AllUsers.Where(o => o.UserType == EUserType.Client);
            InstructroComboBox.ItemsSource = FitnessCenter.Instance.AllUsers.Where(o => o.UserType == EUserType.Instructor);
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            AdminMainWindow admin = new AdminMainWindow();
            admin.Show();
            this.Close();
        }

        private void DeleteTraining_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                object selected = AllTrainings_DataGrid.SelectedItem;
                Training treining = (Training)selected;
                TrainingCRUD.DeleteTrainingFromDatabase(treining);
                FitnessCenter.Instance.AllTrainings.Remove(treining);
                AllTrainings_DataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings;
            } catch
            {
                MessageBox.Show("Please select training");
            }

        }

        private void CreateTraining(object sender, RoutedEventArgs e)
        {
            AddNewTrainingWindow add = new AddNewTrainingWindow();
            add.Show();
            this.Close();
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            Model.Instructor instructor;
            Model.Client client;
            Console.WriteLine(DatePicker.SelectedDate);
            List<Training> trainings = new List<Training>();

            try
            {
                instructor = (SR13_2020_POP2021.Model.Instructor)InstructroComboBox.SelectedItem;
            }
            catch
            {
                instructor = null;
            }

            try
            {
                client = (SR13_2020_POP2021.Model.Client)ClientComboBox.SelectedItem;
            }
            catch
            {
                client = null;
            }

            DateTime date;
            try
            {
                date = (DateTime)DatePicker.SelectedDate;
            }
            catch
            {
                date = new DateTime();
            }

            Console.WriteLine(date.Date);

            foreach (Training item in FitnessCenter.Instance.AllTrainings)
            {
               
                if ((item.Client == client || client == null) && (date == item.Date.Date || date == new DateTime()) && (item.Instructor == instructor || instructor == null))
                {
                    trainings.Add(item);
                }
            }
            AllTrainings_DataGrid.ItemsSource = trainings;
        }

        private void EditTraining(object sender, RoutedEventArgs e)
        {
            Training training = (Training)AllTrainings_DataGrid.SelectedItem;
            if(training != null)
            {
                EditTrainingWindow edit = new EditTrainingWindow(training);
                edit.Show();
                this.Close();
            } else
            {
                MessageBox.Show("Please select training");
            }
            
        }
    }
}

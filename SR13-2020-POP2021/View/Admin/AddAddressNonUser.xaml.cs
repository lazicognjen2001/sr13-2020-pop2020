﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Admin
{
    /// <summary>
    /// Interaction logic for AddAddressNonUser.xaml
    /// </summary>
    public partial class AddAddressNonUser : Window
    {

        Adress address;
        public AddAddressNonUser()
        {
            InitializeComponent();
            address = new Adress();
            this.DataContext = address;
        }

        private void AddAddress(object sender, RoutedEventArgs e)
        {

            string State = StateField.Text;
            string City = CityField.Text;
            string Street = StreetField.Text;
            string Number = NumberField.Text;

            if (isValid())
            {
            Adress clientAdress = null;
            int newAddressID = 0;
            bool addressExist = false;
            foreach (Adress address in FitnessCenter.Instance.AllAdresses)
            {
                if (State.Equals(address.State) && City.Equals(address.City) && Street.Equals(address.Street) && int.Parse(Number) == address.AdressNumber)
                {
                    clientAdress = address;
                    addressExist = true;
                    break;
                }
                else
                {
                    if (address.Id > newAddressID)
                    {
                        newAddressID = address.Id + 1;
                    }
                }
            }

            if (!addressExist)
            {
                //clientAdress = new Model.Adress(newAddressID, State, City, Street, int.Parse(Number));
                AddressCRUD.AddNewAddressInDatabase(address);
            } else
            {
                MessageBox.Show("Address already exists in system");
            }
            }


        }
        private void Back(object sender, RoutedEventArgs e)
        {
                AllAddresses ad = new AllAddresses();
                ad.Show();
                this.Close();
        }

        private bool isValid()
        {
            bool valid = !System.Windows.Controls.Validation.GetHasError(StateField)
                && !System.Windows.Controls.Validation.GetHasError(StreetField)
                && !System.Windows.Controls.Validation.GetHasError(CityField)
                && !System.Windows.Controls.Validation.GetHasError(NumberField);
            return valid;
        }
    }
}

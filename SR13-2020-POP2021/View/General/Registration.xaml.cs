﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.View.Client;

namespace SR13_2020_POP2021.View.General
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        Model.Client newUser;
        public Registration()
        {
            InitializeComponent();
            newUser = new Model.Client();
            this.DataContext = newUser;
        }

        public void Register(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Register");


            /* TODO:
             * 1. Preuzeti podatke koji su uneseni u formu x
             * 2. Provjeriti da li su uneseni svi podaci 
             * 3. Od podataka kkreirati novi objekat x
             * 4. Objekat dodati u listu objekata FitnessCenter.Instance x
             * 5. Upisati objekat u bazu podataka x
             */

            // Nedostaje validacija unesenih podataka

            /*
            string Name = NameField.Text;
            string Surname = SurnameField.Text;
            string Email = EmailField.Text;
            string FirstPassword = FirstPasswordField.Password.ToString();
            string SecondPassword = SecondPasswordField.Password.ToString();
            string Id = IdField.Text;
            */

            string FirstPassword = FirstPasswordField.Password.ToString();
            string SecondPassword = SecondPasswordField.Password.ToString();

            if (FirstPassword.Equals(SecondPassword))
            {
                if (isValid())
                {

                    try
                    {

                        string State = StateField.Text;
                        string City = CityField.Text;
                        string Street = StreetField.Text;
                        string Number = NumberField.Text;
                        EGender Gender = EGender.Male;

                        if ((bool)MaleRadioButton.IsChecked)
                        {
                            Gender = EGender.Male;
                        }
                        else if ((bool)FemaleRadioButton.IsChecked)
                        {
                            Gender = EGender.Female;
                        }

                        //long IDLong = long.Parse(Id);
                        Adress clientAdress = null;
                        int newAddressID = 0;
                        bool addressExist = false;
                        foreach (Adress address in FitnessCenter.Instance.AllAdresses)
                        {
                            if (State.Equals(address.State) && City.Equals(address.City) && Street.Equals(address.Street) && int.Parse(Number) == address.AdressNumber)
                            {
                                clientAdress = address;
                                addressExist = true;
                                break;
                            }
                            else
                            {
                                if (address.Id >= newAddressID)
                                {
                                    newAddressID = address.Id + 1;
                                }
                            }
                        }
                        if (!addressExist)
                        {
                            clientAdress = new Model.Adress(newAddressID, State, City, Street, int.Parse(Number));
                            AddressCRUD.AddNewAddressInDatabase(clientAdress);
                        }

                        newUser.Gender = Gender;
                        newUser.Adress = clientAdress;
                        newUser.IsActive = true;
                        newUser.UserType = EUserType.Client;
                        newUser.Password = FirstPasswordField.Password;
                        newUser.AllTrainings = new System.Collections.ObjectModel.ObservableCollection<Training>();

                        //Model.Client NewClient = new Model.Client(IDLong, Name, Surname, Email, FirstPassword, Gender, EUserType.Client, clientAdress, true);
                        newUser.AddressId = clientAdress.Id;
                        Console.WriteLine(clientAdress.ToString());
                        FitnessCenter.Instance.AllUsers.Add(newUser);
                        FitnessCenter.Instance.CurrentUser = newUser;
                        UserCRUD.AddNewUserInDatabase(newUser);

                        ClientMainWindow main = new ClientMainWindow();
                        main.Show();
                        this.Close();
                    }
                    catch (Exception)
                    {

                        MessageBox.Show("Data is not valid, please try again");
                    }

                }
            } else
            {
                MessageBox.Show("Passwords are not the same");
            }

            // Nedostaje validacija unesenih podataka
        }

        public void Back(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private bool isValid()
        {
            bool valid = !System.Windows.Controls.Validation.GetHasError(EmailField) 
                && !System.Windows.Controls.Validation.GetHasError(NameField)
                && !System.Windows.Controls.Validation.GetHasError(SurnameField)
                && !System.Windows.Controls.Validation.GetHasError(IdField);
            return valid;
        }
    }
}

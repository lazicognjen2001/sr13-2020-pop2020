﻿using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.View.Client;
using SR13_2020_POP2021.View.Guest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.General
{
    /// <summary>
    /// Interaction logic for InstructorSearch.xaml
    /// </summary>
    public partial class InstructorSearch : Window
    {
        public InstructorSearch()
        {
            InitializeComponent();
            var allUsers = FitnessCenter.Instance.AllUsers.Where(o => o.IsActive = true);
            AllUsersDataGrid.ItemsSource = allUsers.Where(o => o.UserType == EUserType.Instructor);

            _ = GenderCombo.Items.Add("Male");
            _ = GenderCombo.Items.Add("Female");

            AddressComboBox.ItemsSource = FitnessCenter.Instance.AllAdresses;

        }

        private void Back(object sender, RoutedEventArgs e)
        {
            if (FitnessCenter.Instance.CurrentUser != null)
            {
                ClientMainWindow client = new ClientMainWindow();
                client.Show();
                this.Close();
            } else
            {
                GuestMainWindow guest = new GuestMainWindow();
                guest.Show();
                this.Close();
            }
           
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            string name = NameField.Text;
            string surname = SurnameField.Text;
            string email = EmailField.Text;

            List<User> findedUsers = new List<User>();


            foreach (User user in FitnessCenter.Instance.AllUsers)
            {

                bool info = (user.Name.Equals(name) || name == "") &&
                    (user.Surname.Equals(surname) || surname == "") &&
                    (user.Email.Equals(email) || email == "");



                bool gender = user.Gender.ToString().Equals(GenderCombo.SelectedItem) || GenderCombo.SelectedItem == null;
                bool address = (user.Adress == (Adress)AddressComboBox.SelectedItem) || AddressComboBox.SelectedItem == null;



                if (
                    info && gender && address
                    )
                {
                    findedUsers.Add(user);
                }
            }

            AllUsersDataGrid.ItemsSource = findedUsers.Where(o => o.UserType == EUserType.Instructor);
        }
    }
}

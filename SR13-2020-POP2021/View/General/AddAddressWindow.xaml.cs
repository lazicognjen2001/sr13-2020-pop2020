﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.View.Admin;
using SR13_2020_POP2021.View.Client;
using SR13_2020_POP2021.View.Instructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.General
{
    /// <summary>
    /// Interaction logic for AddAddressWindow.xaml
    /// </summary>
    public partial class AddAddressWindow : Window
    {
        public AddAddressWindow()
        {
            InitializeComponent();
        }

        private void AddAddress(object sender, RoutedEventArgs e)
        {
            
            string State = StateField.Text;
            string City = CityField.Text;
            string Street = StreetField.Text;
            string Number = NumberField.Text;

            Adress clientAdress = null;
            int newAddressID = 0;
            bool addressExist = false;
            foreach (Adress address in FitnessCenter.Instance.AllAdresses)
            {
                if (State.Equals(address.State) && City.Equals(address.City) && Street.Equals(address.Street) && int.Parse(Number) == address.AdressNumber)
                {
                    clientAdress = address;
                    addressExist = true;
                    break;
                }
                else
                {
                    if (address.Id > newAddressID)
                    {
                        newAddressID = address.Id + 1;
                    }
                }
            }

            if (!addressExist)
            {
                clientAdress = new Model.Adress(newAddressID, State, City, Street, int.Parse(Number));
                AddressCRUD.AddNewAddressInDatabase(clientAdress);
            }

            FitnessCenter.Instance.CurrentUser.AddressId = clientAdress.Id;
            FitnessCenter.Instance.CurrentUser.Adress = clientAdress;

        }
        private void Back(object sender, RoutedEventArgs e)
        {
            if(FitnessCenter.Instance.CurrentUser.UserType == EUserType.Admin)
            {
                AdminSettingsWindow ad = new AdminSettingsWindow();
                ad.Show();
                this.Close();
            } else if (FitnessCenter.Instance.CurrentUser.UserType == EUserType.Instructor)
            {
                InstructorSettingsWindow ad = new InstructorSettingsWindow();
                ad.Show();
                this.Close();
            } else if (FitnessCenter.Instance.CurrentUser.UserType == EUserType.Admin)
            {
                ClientSettingsWindow ad = new ClientSettingsWindow();
                ad.Show();
                this.Close();
            }

        }

    }
}

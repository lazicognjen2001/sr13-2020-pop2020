﻿using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.View.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Client
{
    /// <summary>
    /// Interaction logic for ClientMainWindow.xaml
    /// </summary>
    public partial class ClientMainWindow : Window
    {
        public ClientMainWindow()
        {
            InitializeComponent();
            NameAndSurname.Content = FitnessCenter.Instance.CurrentUser.Name + " " + FitnessCenter.Instance.CurrentUser.Surname;
        }

        public void ReserveNewTraining(object sender, RoutedEventArgs e)
        {
            BookNewTrainingWindow book = new BookNewTrainingWindow();
            book.Show();
            this.Close();
        }

        public void ReservedTrainings(object sender, RoutedEventArgs e)
        {
            AllTrainingsClientWindow allTrainingsClientWindow = new AllTrainingsClientWindow();
            allTrainingsClientWindow.Show();
            this.Close();
               

        }

        public void Settings(object sender, RoutedEventArgs e)
        {
            ClientSettingsWindow clientSettingsWindow = new ClientSettingsWindow();
            clientSettingsWindow.Show();
            this.Close();

        }

        public void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void AllInstructors(object sender, RoutedEventArgs e)
        {
            InstructorSearch ins = new InstructorSearch();
            ins.Show();
            this.Close();
        }
    }
}

﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Client
{
    /// <summary>
    /// Interaction logic for BookNewTrainingWindow.xaml
    /// </summary>
    public partial class BookNewTrainingWindow : Window
    {
        public BookNewTrainingWindow()
        {
            InitializeComponent();
            Model.Client client = (Model.Client)FitnessCenter.Instance.CurrentUser;
            var freeTrainings = FitnessCenter.Instance.AllTrainings.Where(o => o.TrainingStatus == ETrainingStatus.Free);
            DateTime localDate = DateTime.Now;
            AllTrainingsDataGrid.ItemsSource = freeTrainings.Where(o => o.Date > localDate);
            InstructorCombo.ItemsSource = FitnessCenter.Instance.AllUsers.Where(i => i.UserType == EUserType.Instructor);

            foreach (Training item in FitnessCenter.Instance.AllTrainings)
            {
                Console.WriteLine(item.TrainingStatus);
            }
            
            
        }

        private void BookTraining_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                object selected = AllTrainingsDataGrid.SelectedItem;
                Training treining = (Training)selected;
                Model.Client client = (Model.Client)FitnessCenter.Instance.CurrentUser;
                var freeTrainings = FitnessCenter.Instance.AllTrainings.Where(o => o.TrainingStatus == ETrainingStatus.Free);
                DateTime localDate = DateTime.Now;

                if (treining.Client == null)
                {
                    treining.Client = client;
                    treining.ClientId = client.Id;
                    treining.TrainingStatus = ETrainingStatus.Booked;
                    TrainingCRUD.UpdateTrainingInDatabase(treining);
                    //FitnessCenter.Instance.AllTrainings
                    AllTrainingsDataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings;
                    client.AllTrainings.Add(treining);
                    AllTrainingsDataGrid.ItemsSource = freeTrainings.Where(o => o.Date > localDate);
                } else
                {
                    AllTrainingsDataGrid.ItemsSource = freeTrainings.Where(o => o.Date > localDate);
                }

            } catch
            {
                MessageBox.Show("Please select training");
            }
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            Model.Instructor instructor;
            List<Training> trainings = new List<Training>();

            try
            {
                instructor = (Model.Instructor)InstructorCombo.SelectedItem;
            }
            catch
            {
                instructor = null;
            }

            DateTime date;
            try
            {
                date = (DateTime)DatePicker.SelectedDate;
            }
            catch
            {
                date = new DateTime();
            }

            bool allTrinnings = (bool)BookedAndGFreeCheckbox.IsChecked;
            bool pastAndFuture = (bool)PastAndFutureTrainings.IsChecked;
            Console.WriteLine(allTrinnings);

            foreach (Training item in FitnessCenter.Instance.AllTrainings)
            {
                if (!allTrinnings) { 
                    if ((item.Instructor == instructor || instructor == null) && (date == item.Date.Date || date == new DateTime()) && (item.TrainingStatus == ETrainingStatus.Free))
                    {
                        trainings.Add(item);
                    }
                } else
                {
                    if ((item.Instructor == instructor || instructor == null) && (date == item.Date.Date || date == new DateTime()))
                    {
                        trainings.Add(item);
                    }
                }
            }
            DateTime localDate = DateTime.Now;

            if (!pastAndFuture)
            {
                AllTrainingsDataGrid.ItemsSource = trainings.Where(o => o.Date > localDate); ;
            }
            else
            {
                AllTrainingsDataGrid.ItemsSource = trainings;
            }
            //List<Training> trainingsFilter = trainings.Where(o => o.Date > localDate);
            //AllTrainingsDataGrid.ItemsSource = trainings;

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            ClientMainWindow client = new ClientMainWindow();
            client.Show();
            this.Close();
        }
    }
}

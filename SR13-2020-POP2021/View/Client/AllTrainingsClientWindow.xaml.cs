﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Client
{
    /// <summary>
    /// Interaction logic for AllTrainingsClientWindow.xaml
    /// </summary>
    public partial class AllTrainingsClientWindow : Window
    {
        public AllTrainingsClientWindow()
        {
            InitializeComponent();
            Model.Client client = (Model.Client)FitnessCenter.Instance.CurrentUser;
            AllTrainingsDataGrid.ItemsSource = client.AllTrainings;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ClientMainWindow client = new ClientMainWindow();
            client.Show();
            this.Close();
        }

        private void DeclineTraining(object sender, RoutedEventArgs e)
        {
            try
            {
                object selected = AllTrainingsDataGrid.SelectedItem;
                Training treining = (Training)selected;
                Model.Client client = (Model.Client)FitnessCenter.Instance.CurrentUser;
                treining.Client = null;
                treining.ClientId = 0;
                treining.TrainingStatus = ETrainingStatus.Free;
                TrainingCRUD.UpdateTrainingInDatabase(treining);
                //FitnessCenter.Instance.AllTrainings
                //AllTrainingsDataGrid.ItemsSource = FitnessCenter.Instance.AllTrainings;
                client.AllTrainings.Remove(treining);
                AllTrainingsDataGrid.ItemsSource = client.AllTrainings; 
            }
            catch
            {
                MessageBox.Show("Please select training");
            }
        }
    }
}

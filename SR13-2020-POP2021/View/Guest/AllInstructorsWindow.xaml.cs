﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Guest
{
    /// <summary>
    /// Interaction logic for AllInstructorsWindow.xaml
    /// </summary>
    public partial class AllInstructorsWindow : Window
    {
        public AllInstructorsWindow()
        {
            InitializeComponent();
            var instructors = FitnessCenter.Instance.AllUsers.Where(o => o.UserType == EUserType.Instructor);
            AllInstructorsDataGrid.ItemsSource = instructors.Select(o => new {Name = o.Name, Surname  = o.Surname, Email = o.Email, Gender = o.Gender}).ToList();

        }

        private void Back(object sender, RoutedEventArgs e)
        {
            GuestMainWindow guest = new GuestMainWindow();
            guest.Show();
            this.Close();
        }
    }
}

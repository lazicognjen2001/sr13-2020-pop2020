﻿using SR13_2020_POP2021.View.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Guest
{
    /// <summary>
    /// Interaction logic for GuestMainWindow.xaml
    /// </summary>
    public partial class GuestMainWindow : Window
    {
        public GuestMainWindow()
        {
            InitializeComponent();
        }

        public void OurInstructors(object sender, RoutedEventArgs e)
        {
            InstructorSearch all = new InstructorSearch();
            all.Show();
            this.Close();

        }

        public void AboutUs(object sender, RoutedEventArgs e)
        {
            FitnessCenterInformationWindow fitnessCenterInformationWindow = new FitnessCenterInformationWindow();
            fitnessCenterInformationWindow.Show();
            this.Close();
        }

        public void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}

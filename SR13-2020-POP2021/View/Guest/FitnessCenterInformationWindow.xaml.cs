﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR13_2020_POP2021.View.Guest
{
    /// <summary>
    /// Interaction logic for FitnessCenterInformationWindow.xaml
    /// </summary>
    public partial class FitnessCenterInformationWindow : Window
    {
        public FitnessCenterInformationWindow()
        {
            InitializeComponent();
            NameOfFitnessCenter.Content = FitnessCenter.Instance.Name;
            FitnessCenterId.Content = FitnessCenter.Instance.Id;
            FitnessCenterAddress.Content = FitnessCenter.Instance.Adress;

        }

        private void Back(object sender, RoutedEventArgs e)
        {
            GuestMainWindow guestMainWindow = new GuestMainWindow();
            guestMainWindow.Show();
            this.Close();
        }
    }
}

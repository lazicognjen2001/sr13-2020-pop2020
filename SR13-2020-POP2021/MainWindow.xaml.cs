﻿using SR13_2020_POP2021.Helpers;
using SR13_2020_POP2021.Model;
using SR13_2020_POP2021.View.Admin;
using SR13_2020_POP2021.View.Client;
using SR13_2020_POP2021.View.General;
using SR13_2020_POP2021.View.Guest;
using SR13_2020_POP2021.View.Instructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SR13_2020_POP2021
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FitnessCenter FitnessCenter;
        public MainWindow()
        {
            InitializeComponent();
            FitnessCenter centar = FitnessCenter.Instance;
            FitnessCenterCRUD.GetData();
            FitnessCenter = centar;
        }


        private void Login(object sender, RoutedEventArgs e)
        {
            User loggedUser = FindLogIn();
            if (loggedUser is Admin)
            {
                Admin admin = (Admin)loggedUser;
                FitnessCenter.Instance.CurrentUser = admin;
                AdminMainWindow AdminWindow = new AdminMainWindow();
                AdminWindow.Show();
                this.Hide();
            } else if (loggedUser is Instructor)
            {
                Instructor instructor = (Instructor)loggedUser;
                FitnessCenter.Instance.CurrentUser = instructor;
                InstructorMainWindow InstructorWindow = new InstructorMainWindow();
                InstructorWindow.Show();
                this.Hide();
            }
            else if (loggedUser is Client)
            {
                Client client = (Client)loggedUser;
                FitnessCenter.Instance.CurrentUser = client;
                ClientMainWindow CliendWindow = new ClientMainWindow();
                CliendWindow.Show();
                this.Hide();
            }
            else if (loggedUser is null)
            {
                MessageBox.Show("There is no required user, please try again.");
            } else
            {
                MessageBox.Show("Error while running program, please try again later.");
            }
        }

        private User FindLogIn()
        {
            foreach (User user in FitnessCenter.AllUsers)
            {
                Console.WriteLine(user.Email.Equals(email.Text) && user.Password.Equals(passwordBoxic.Password.ToString()));
                if (user.Email.Equals(email.Text) && user.Password.Equals(passwordBoxic.Password.ToString()))
                {
                    return user;
                }
            }
            return null;
        }

        public void Register(object sender, RoutedEventArgs e)
        {
            Registration Registration = new Registration();
            Registration.Show();
            this.Hide();
        }

        public void LogInAsGuest(object sender, RoutedEventArgs e)
        {
            FitnessCenter.Instance.CurrentUser = null; 
            GuestMainWindow guestWindow = new GuestMainWindow();
            guestWindow.Show();
            this.Hide();
        }

    }
}

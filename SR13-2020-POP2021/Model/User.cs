﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Model
{
    public class User
    {
        public User() { }
        public User(long Id,
                    string Name, 
                    string Surname,
                    string Email,
                    string Password,
                    EGender Gender,
                    EUserType UserType,
                    Adress Adress,
                    bool IsActive)
        {
            this._id = Id;
            this._name = Name;
            this._surname = Surname;
            this._email = Email;
            this._password = Password;
            this._gender = Gender;
            this._userType = UserType;
            this._adress = Adress;
            this._isActive = IsActive;
        }

        

        private string _name = "";
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _surname;
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private long _id;
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private Adress _adress;
        public Adress Adress
        {
            get { return _adress; }
            set { _adress = value; }
        }

        private EGender _gender;
        public EGender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private EUserType _userType;

        public EUserType UserType
        {
            get { return _userType; }
            set { _userType = value; }
        }

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }


        private int _addressId;

        public int AddressId
        {
            get { return _addressId; }
            set { _addressId = value; }
        }

        


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Model
{
    class Admin : User
    {
        public Admin(long Id,
                    string Name,
                    string Surname,
                    string Email,
                    string Password,
                    EGender Gender,
                    EUserType UserType,
                    Adress Adress,
                    bool IsActive) : base(Id, Name, Surname, Email, Password, Gender, UserType, Adress, IsActive)
        {
           
        }


    }
}

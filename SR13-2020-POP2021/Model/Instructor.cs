﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Model
{
    class Instructor : User
    {
        public Instructor(long Id,
                    string Name,
                    string Surname,
                    string Email,
                    string Password,
                    EGender Gender,
                    EUserType UserType,
                    Adress Adress,
                    bool IsActive) : base(Id, Name, Surname, Email, Password, Gender, UserType, Adress, IsActive)
        {

        }

        private ObservableCollection<Training> _allTrainings = new ObservableCollection<Training>();

        public ObservableCollection<Training> AllTrainings
        {
            get { return _allTrainings; }
            set { _allTrainings = value; }
        }
        override
        public string ToString()
        {
            return base.Name;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Model
{

    
    public class Adress
    {

        public Adress() { }
        public Adress(
                int Id,
                string State,
                string City,
                string Street,
                int AdressNumber
            )
        {
            this._id = Id;
            this._state = State;
            this._city = City;
            this._street = Street;
            this._adressNumber = AdressNumber;
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _street;

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }


        private int _adressNumber;

        public int AdressNumber
        {
            get { return _adressNumber; }
            set { _adressNumber = value; }
        }


        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }


        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        override
        public string ToString()
        {
            return   _city + " " + _state + " " + _street + " " + _adressNumber + " ";
        }
    }
}

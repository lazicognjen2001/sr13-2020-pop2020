﻿using SR13_2020_POP2021.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SR13_2020_POP2021.Model
{
    public class FitnessCenter
    {
        private FitnessCenter()
        {
            this._allUsers = UserCRUD.GetData();
            this._allTrainings = TrainingCRUD.GetData();
            this._allAddresses = AddressCRUD.GetData();
            UpdateLists();
        }



        private static readonly object podlock = new object();
        private static FitnessCenter instance = null;
        public static FitnessCenter Instance
        {
            get
            {
                lock (podlock)
                    {
                        if (instance == null)
                        {
                            instance = new FitnessCenter();
                        }
                        return instance;
                    }
            }
        }


        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private Adress _adress;

        public Adress Adress
        {
            get { return _adress; }
            set { _adress = value; }
        }

        private ObservableCollection<User> _allUsers;

        public ObservableCollection<User> AllUsers
        {
            get { return _allUsers; }
            set { _allUsers = value; }
        }

        private ObservableCollection<Adress> _allAddresses;

        public ObservableCollection<Adress> AllAdresses
        {
            get { return _allAddresses; }
            set { _allAddresses = value; }
        }

        private ObservableCollection<Training> _allTrainings;

        public ObservableCollection<Training> AllTrainings      
        {
            get { return _allTrainings; }
            set { _allTrainings = value; }
        }

        private User _currentUser;

        public User CurrentUser
        {
            get { return _currentUser; }
            set { _currentUser = value; }
        }


        private void UpdateLists()
        {
            foreach (User user in _allUsers)
            {
                if (user is Instructor)
                {
                    Instructor instructor = (Instructor)user;
                    foreach(Training training in _allTrainings)
                    {
                        if(instructor.Id == training.InstructorId)
                        {
                            instructor.AllTrainings.Add(training);
                            training.Instructor = instructor;
                        }
                    }
                } else if (user is Client)
                {
                    Client client = (Client)user;
                    foreach (Training training in _allTrainings)
                    {
                        if (client.Id == training.ClientId)
                        {
                            client.AllTrainings.Add(training);
                            training.Client = client;
                        }
                    }
                }

                foreach (Adress adress in _allAddresses)
                {
                    if(adress.Id == user.AddressId)
                    {
                        user.Adress = adress;
                    }
                }

            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Model
{
    public class Training
    {

        public Training(
            int Id,
            User Client,
            User Instructor,
            DateTime DateTime,
            ETrainingStatus TrainingStatus
            )
        {
            this._id = Id;
            this._client = Client;
            this._instructor = Instructor;
            this._date = DateTime;
            this._trainingStatus = TrainingStatus;
        }


        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }


        private User _instructor;

        public User Instructor
        {
            get { return _instructor; }
            set { _instructor = value; }
        }

        private User _client = new Client();

        public User Client
        {
            get { return _client; }
            set { _client = value; }
        }

        private ETrainingStatus _trainingStatus;

        public ETrainingStatus TrainingStatus

        {
            get { return _trainingStatus; }
            set { _trainingStatus = value; }
        }

        private Int64 _instructorId;

        public Int64 InstructorId
        {
            get { return _instructorId; }
            set { _instructorId = value; }
        }

        private Int64 _clientId;

        public Int64 ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

       

    }
   
}

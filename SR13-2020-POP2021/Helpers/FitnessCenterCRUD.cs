﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Helpers
{
    class FitnessCenterCRUD
    {
        public int id;
        public string name;
        public Adress address;
        public static void GetData()
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * From GeneralInfo";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Adress centerAddress = null;
                    foreach (Adress address in FitnessCenter.Instance.AllAdresses)
                    {
                        if (address.Id == (int)reader.GetValue(2))
                        {
                            centerAddress = address;
                        }
                    }

                    FitnessCenter.Instance.Id = (int)reader.GetValue(0);
                    FitnessCenter.Instance.Name = (string)reader.GetValue(1);
                    FitnessCenter.Instance.Adress = centerAddress;
                }
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database, Adress class");
            }
        }

        public static void UpdateFittnessCenterInDatabase()
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update GeneralInfo Set Id=@Id, Name=@Name, AddressId=@AddressId Where Id = @Id", connection);

                command.Parameters.AddWithValue("@Id", FitnessCenter.Instance.Id);
                command.Parameters.AddWithValue("@Name", FitnessCenter.Instance.Name);
                command.Parameters.AddWithValue("@AddressId", FitnessCenter.Instance.Id);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        } 

    }
}

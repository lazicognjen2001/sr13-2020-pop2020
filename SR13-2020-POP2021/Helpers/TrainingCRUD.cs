﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Helpers
{
    class TrainingCRUD
    {
        public static ObservableCollection<Training> GetData()
        {
            ObservableCollection<Training> trainingCollection = new ObservableCollection<Training>();
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * From Trainings";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if ((bool)reader.GetValue(5) == true) { 
                    string trainingStatusString = (string)reader.GetValue(4);
                    ETrainingStatus trainingStatus = ETrainingStatus.Free;

                    if (trainingStatusString.Equals("Free"))
                    {
                        trainingStatus = ETrainingStatus.Free;
                    } else if (trainingStatusString.Equals("Booked"))
                    {
                        trainingStatus = ETrainingStatus.Booked;
                    }

                    Training newTraining = new Training(
                        (int)reader.GetValue(0),
                        null,
                        null,
                        (DateTime)reader.GetValue(3),
                        trainingStatus
                        );

                    try
                    {
                        newTraining.ClientId = (Int64)reader.GetValue(1);
                    }
                    catch (Exception)
                    {

                        newTraining.ClientId = 0;
                    }

                    newTraining.InstructorId = (Int64)reader.GetValue(2);
                    trainingCollection.Add(newTraining);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database, Training class");
            }
            return trainingCollection;
        }

        public static void AddNewTrainingInDatabase(Training newTraining)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                string id = "Null";
                if (newTraining.Client == null)
                {
                    id = "Null";
                }
                else
                {
                    id = newTraining.Client.Id.ToString();
                }

                string sqlFormattedDate = newTraining.Date.ToString("yyyy-MM-dd HH:mm");

                SqlCommand command = new SqlCommand("Insert into Trainings Values ("+ newTraining.Id + ", " + id + ", " + newTraining.Instructor.Id + ", '" + sqlFormattedDate + "', '" + newTraining.TrainingStatus.ToString() + "', " + 1 + ")", connection);

                
                Console.WriteLine(sqlFormattedDate);
                Console.WriteLine(newTraining.Id);
                Console.WriteLine(id);
                Console.WriteLine(newTraining.Instructor.Id);
                Console.WriteLine(newTraining.TrainingStatus.ToString());
                Console.WriteLine(1);



                command.Parameters.AddWithValue("@Id", newTraining.Id);
                command.Parameters.AddWithValue("@UserId", id);
                command.Parameters.AddWithValue("@InstructorId", newTraining.Instructor.Id);
                command.Parameters.AddWithValue("@TrainingTime", sqlFormattedDate);
                command.Parameters.AddWithValue("@TrainingStatus", newTraining.TrainingStatus.ToString());
                command.Parameters.AddWithValue("@IsActive", 1);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

        public static void DeleteTrainingFromDatabase(Training training)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update Trainings Set IsActive = @IsActive Where Id=@Id", connection);

                command.Parameters.AddWithValue("@Id", training.Id);
                command.Parameters.AddWithValue("@IsActive", 0);

                command.ExecuteNonQuery();
                connection.Close();
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

        public static void UpdateTrainingInDatabase(Training training)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                string Id = "Null";
                if (training.Client != null)
                {
                    Id = training.Client.Id.ToString();
                }

                string sqlFormattedDate = training.Date.ToString("yyyy-MM-dd HH:mm");


                SqlCommand command = new SqlCommand("Update Trainings Set Id=" + training.Id + ", UserId=" + Id + ", InstructorId=" + training.Instructor.Id + ", TrainingTime='" + sqlFormattedDate + "', TrainingStatus='" + training.TrainingStatus.ToString() + "', IsActive=" + 1 + " Where Id = " + training.Id + "", connection);

                

                command.Parameters.AddWithValue("@Id", training.Id);
                command.Parameters.AddWithValue("@UserId", Id);
                command.Parameters.AddWithValue("@InstructorId", training.Instructor.Id);
                command.Parameters.AddWithValue("@TrainingTime", training.Date);
                command.Parameters.AddWithValue("@TrainingStatus", training.TrainingStatus.ToString());
                command.Parameters.AddWithValue("@IsActive", 1);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }
    }
}


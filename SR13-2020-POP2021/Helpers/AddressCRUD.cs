﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR13_2020_POP2021.Helpers
{
    class AddressCRUD
    {
        public static ObservableCollection<Adress> GetData()
        {
            ObservableCollection<Adress> addressCollection = new ObservableCollection<Adress>();
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * From Addresses";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Adress newAdress = new Adress(
                        (int)reader.GetValue(0),
                        (string)reader.GetValue(1),
                        (string)reader.GetValue(2),
                        (string)reader.GetValue(3),
                        (int)reader.GetValue(4)
                        );

                    addressCollection.Add(newAdress);
                }
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database, Adress class");
            }
            return addressCollection;
        }


        public static void AddNewAddressInDatabase(Adress newAddress)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into Addresses Values ("+ newAddress.Id + ", '" + newAddress.State + "', '" + newAddress.City + "', '" + newAddress.Street + "', " + newAddress.AdressNumber + ", 1)", connection);
                // Insert into Addresses Values (@Id, @Country, @City, @Adress, @PostalCode, @IsActive)"
                _ = command.Parameters.AddWithValue("@Id", newAddress.Id);
                _ = command.Parameters.AddWithValue("@Country", newAddress.State);
                _ = command.Parameters.AddWithValue("@City", newAddress.City);
                _ = command.Parameters.AddWithValue("@Adress", newAddress.Street);
                _ = command.Parameters.AddWithValue("@PostalCode", newAddress.AdressNumber);
                _ = command.Parameters.AddWithValue("@IsActive", 1);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

        public static void DeleteAddressFromDatabase(Adress address)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update Addresses Set IsActive = @IsActive Where Id=@Id", connection);

                _ = command.Parameters.AddWithValue("@Id", address.Id);
                _ = command.Parameters.AddWithValue("@IsActive", 0);

                _ = command.ExecuteNonQuery();
                connection.Close();
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }

        }

        public static void UpdateAddressInDatabase(Adress address)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update Addresses Set Id=@Id, Country=@Country, City=@City, Adress=@Adress, PostalCode=@PostalCode, IsActive=@IsActive Where Id = @Id", connection);

                _ = command.Parameters.AddWithValue("@Id", address.Id);
                _ = command.Parameters.AddWithValue("@Country", address.State);
                _ = command.Parameters.AddWithValue("@City", address.City);
                _ = command.Parameters.AddWithValue("@Adress", address.Street);
                _ = command.Parameters.AddWithValue("@PostalCode", address.AdressNumber);
                _ = command.Parameters.AddWithValue("@IsActive", 1);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

    }
}


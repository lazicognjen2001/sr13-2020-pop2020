﻿using SR13_2020_POP2021.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using SR13_2020_POP2021.Interfaces;

namespace SR13_2020_POP2021.Helpers
{
    class UserCRUD
    {
        public static ObservableCollection<User> GetData()
        {
            ObservableCollection<User> userCollection = new ObservableCollection<User>();
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "Select * From Users";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    bool isActive = (bool)reader.GetValue(8);
                    if (isActive)
                    {

                        string UserType = (string)reader.GetValue(6);

                        string GenderString = (string)reader.GetValue(5);
                        EGender Gender = EGender.Male;
                        Console.WriteLine(reader.GetValue(5));
                        if (GenderString.Equals("Male"))
                        {
                            Gender = EGender.Male;
                        }
                        else if (GenderString.Equals("Female"))
                        {
                            Gender = EGender.Female;
                        }

                        if (UserType.Equals("Client"))
                        {
                            Client newClient = new Client(
                                (Int64)reader.GetValue(0),
                                (string)reader.GetValue(1),
                                (string)reader.GetValue(2),
                                (string)reader.GetValue(3),
                                (string)reader.GetValue(4),
                                Gender,
                                EUserType.Client,
                                null, // Adress field
                                (bool)reader.GetValue(8)
                                );
                            newClient.AddressId = (int)reader.GetValue(7);
                            userCollection.Add(newClient);

                        }
                        else if (UserType.Equals("Instructor"))
                        {

                            Instructor newInstructor = new Instructor(
                                (Int64)reader.GetValue(0),
                                (string)reader.GetValue(1),
                                (string)reader.GetValue(2),
                                (string)reader.GetValue(3),
                                (string)reader.GetValue(4),
                                Gender,
                                EUserType.Instructor,
                                null, // Adress field
                                (bool)reader.GetValue(8)
                                );
                            newInstructor.AddressId = (int)reader.GetValue(7);
                            userCollection.Add(newInstructor);
                        }
                        else if (UserType.Equals("Admin"))
                        {

                            Admin newAdmin = new Admin(
                                (Int64)reader.GetValue(0),
                                (string)reader.GetValue(1),
                                (string)reader.GetValue(2),
                                (string)reader.GetValue(3),
                                (string)reader.GetValue(4),
                                Gender,
                                EUserType.Admin,
                                null, // Adress field
                                (bool)reader.GetValue(8)
                                );
                            newAdmin.AddressId = (int)reader.GetValue(7);
                            userCollection.Add(newAdmin);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
            return userCollection;
        }

        public static void AddNewUserInDatabase(User newUser)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into Users Values ("+ newUser.Id + ", '" + newUser.Name + "', '" + newUser.Surname + "', '" + newUser.Email + "', '" + newUser.Password + "', '" + newUser.Gender.ToString() + "', '" + newUser.UserType.ToString() + "', " + newUser.AddressId + ", 1)", connection);
                //"Insert into Users Values (@Id, @Ime, @Prezime, @Email, @Password, @Gender, @UserType, @AddressId, @IsActive)"
                command.Parameters.AddWithValue("@Id", newUser.Id);
                command.Parameters.AddWithValue("@Ime", newUser.Name);
                command.Parameters.AddWithValue("@Prezime", newUser.Surname);
                command.Parameters.AddWithValue("@Email", newUser.Email);
                command.Parameters.AddWithValue("@Password", newUser.Password);
                command.Parameters.AddWithValue("@Gender", newUser.Gender.ToString());
                command.Parameters.AddWithValue("@UserType", newUser.UserType.ToString());

                command.Parameters.AddWithValue("@AddressId", newUser.Adress.Id);
                // Ako ne postoji u bazi adresa sa tim id onda se ne može dodati korisnik
                // Zato se prvo dodaje adresa u bazi pa tek onda korisnik

                command.Parameters.AddWithValue("@IsActive", 1);
        
                Console.WriteLine(command.CommandText);
                
                int rows = command.ExecuteNonQuery();
                connection.Close();
                
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

        public static void DeleteUserFromDatabase(User user)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update Users Set IsActive = @IsActive Where Id=@Id", connection);
                
                command.Parameters.AddWithValue("@Id", user.Id);
                command.Parameters.AddWithValue("@IsActive", 0);

                command.ExecuteNonQuery();
                connection.Close();
            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }

        public static void UpdateUserInDatabase(User user)
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Server=DESKTOP-39BVGH4;Database=FitnessCenter;Trusted_Connection=Yes;");
                connection.Open();
                SqlCommand command = new SqlCommand("Update Users Set Id=@Id, Ime=@Ime, Prezime=@Prezime, Email=@Email, Password=@Password, Gender=@Gender, UserType=@UserType, AddressId=@AddressId, IsActive=@IsActive Where Id = @Id", connection);

                command.Parameters.AddWithValue("@Id", user.Id);
                command.Parameters.AddWithValue("@Ime", user.Name);
                command.Parameters.AddWithValue("@Prezime", user.Surname);
                command.Parameters.AddWithValue("@Email", user.Email);
                command.Parameters.AddWithValue("@Password", user.Password);
                command.Parameters.AddWithValue("@Gender", user.Gender.ToString());
                command.Parameters.AddWithValue("@UserType", user.UserType.ToString());
                command.Parameters.AddWithValue("@AddressId", user.Adress.Id);
                command.Parameters.AddWithValue("@IsActive", 1);

                Console.WriteLine(command.CommandText);

                int rows = command.ExecuteNonQuery();
                connection.Close();

            }
            catch
            {
                Console.WriteLine("Error coused while connecting with database");
            }
        }
    }
}
